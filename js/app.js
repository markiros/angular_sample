var module = angular.module('mainapp', ['ngRoute']);

module.controller('MainCtrl', function ($scope) {
    $scope.topnav = [
        {title: 'About', href: '#/about'},
        {title: 'Catalog', href: '#/catalog'},
        {title: 'Todo', href: '#/todo'},
        {title: 'Contacts', href: '#/contacts'}
    ];
});

module.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/about', {
            templateUrl: 'partials/about.html',
            controller: 'AboutCtrl'
        })
        .when('/catalog', {
            templateUrl: 'partials/catalog.html',
            controller: 'CatalogCtrl'
        })
        .when('/todo', {
            templateUrl: 'partials/todo.html',
            controller: 'TodoCtrl'
        })
        .when('/contacts', {
            templateUrl: 'partials/contacts.html',
            controller: 'ContactsCtrl'
        })
        .otherwise({redirectTo: '/about'});
}]);

module.controller('AboutCtrl', function ($scope, $routeParams) {
    console.log('AboutCtrl');
});

module.controller('CatalogCtrl', function ($scope, $routeParams) {
    console.log('CatalogCtrl');
    $scope.products = [
        {id: 1, title: 'Product 1', price: 12.43},
        {id: 2, title: 'Product 2', price: 647.30},
        {id: 3, title: 'Product 3', price: 99.53},
        {id: 4, title: 'Product 4', price: 543.76}
    ];

    $scope.delItem = function (item) {
        $scope.products.splice($scope.products.indexOf(item), 1);
    };
});

module.controller('TodoCtrl', function ($scope, $routeParams) {
    $scope.todos = [
        {text: 'learn angular', done: true},
        {text: 'build an angular app', done: false}
    ];

    $scope.addTodo = function () {
        $scope.todos.push({ text: $scope.todoText, done: false });
        $scope.todoText = '';
    };

    $scope.remaining = function () {
        var count = 0;
        angular.forEach($scope.todos, function (todo) {
            count += todo.done ? 0 : 1;
        });
        return count;
    };

    $scope.archive = function () {
        var oldTodos = $scope.todos;
        $scope.todos = [];
        angular.forEach(oldTodos, function (todo) {
            if (!todo.done) $scope.todos.push(todo);
        });
    };
});

module.controller('ContactsCtrl', function ($scope, $routeParams) {
    console.log('ContactsCtrl');
});
